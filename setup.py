from os.path import dirname, join

from setuptools import find_packages, setup

setup(
    name='runnector',  # название пакета, которое пишут в pip install, не обязательно должно совпадать с
    # названием пакета с кодом, но крайне желательно
    packages=find_packages(),
    version='1.1.3',
    description='Инструмент для параллельного запуска тасков',
    author='@nkotov',
    author_email='nkotov@sdvor.com',
    long_description=open(join(dirname(__file__), 'README.md'), encoding='utf-8').read(),
    keywords='async',  # теги/ключевые слова для проекта, например заказ, е2
    url="some_url",  # Урл проекта в гитлабе с пакетом
    install_requires=[  # Сюда пишем пакеты, которые нужны для работы вашего пакета, как в requirements.txt,
        'pytest >= 3.8.1',
        'pytest-asyncio == 0.10.0'
    ],
)
