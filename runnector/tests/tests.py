import asyncio
from typing import Any, Tuple, Optional, AsyncIterable, NoReturn

import pytest
from runnector import AbstractWorker, AbstractBoss, WorkTask, WorkTaskResult, WorkTaskResultStatus


class BossForTest(AbstractBoss):

    async def prepare_data_for_workers(self, *args, **kwargs) -> AsyncIterable[WorkTask]:
        data = kwargs.get('some_chunkable_data') or []
        for item in data:
            yield WorkTask(task_args=[item])


class WorkerForTest(AbstractWorker):

    work_name = 'WorkerForTest'

    def __init__(self, some_param: int, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.some_param = some_param

    async def work(self, work_task: WorkTask) -> Optional[Tuple[Any, ...]] or NoReturn:
        await asyncio.sleep(.1)
        print(work_task)


class FailingWorker(AbstractWorker):

    work_name = 'FailingWorker'

    def __init__(self, some_param: int, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.some_param = some_param

    async def work(self, work_task: WorkTask) -> Optional[Tuple[Any, ...]] or NoReturn:
        await asyncio.sleep(.1)
        raise Exception('some exception')


class TestWorker:
    #     .spawn(count=10, produce=False, worker_args=[], worker_kwargs={})
    #     .consume(worker2.spawn(count=5, produce=True))
    # ).start_job(

    def test_spawn_workers_group_consumering_and_setup(self):
        w1 = WorkerForTest.spawn(count=3, some_param=2, produces=False)
        w2 = WorkerForTest.spawn(count=2, some_param=1, produces=True)
        w1.consume(w2)
        assert len(w1._workers) == 3
        assert len(w2._workers) == 2
        assert w2.consumers == [w1]
        assert not w1.produces
        assert w2.produces
        assert w2._workers_result_queue == w1._result_queue
        for worker in w1._workers:
            assert worker.some_param == 2
            assert worker._consume_queue == w2._produce_queues[0]
        for worker in w2._workers:
            assert worker.some_param == 1
            assert worker._consume_queue is None

    @pytest.mark.asyncio
    async def test_task_put_in_queue_and_completed(self):
        w1 = WorkerForTest.spawn(count=3, some_param=2, produces=False)
        w2 = WorkerForTest.spawn(count=2, some_param=1, produces=True)
        w1.consume(w2)
        some_task = WorkTask(
            task_args=[1, 2, 3],
            task_kwargs=None
        )
        await w2._pass_task_to_consume_group(some_task)

    @pytest.mark.asyncio
    async def test_two_consumers(self):
        w3 = WorkerForTest.spawn(count=3, some_param=2, produces=False)
        w1 = WorkerForTest.spawn(count=3, some_param=2, produces=False)
        w2 = WorkerForTest.spawn(count=2, some_param=1, produces=True)
        w3.consume(w2)
        w1.consume(w2)
        assert len(w2.consumers) == 2
        assert w1._workers[0]._consume_queue != w3._workers[0]._consume_queue

    @pytest.mark.asyncio
    async def test_boss_setup(self):
        boss = BossForTest()
        w1 = WorkerForTest.spawn(count=3, some_param=2, produces=False)
        w2 = WorkerForTest.spawn(count=2, some_param=1, produces=False)
        boss.setup_workers(w1, w2)
        assert len(boss._initial_workers_groups) == 2

    @pytest.mark.asyncio
    async def test_boss_chunk_no_tasks_no_dead_lock(self):
        boss = BossForTest()
        w1 = WorkerForTest.spawn(count=3, some_param=2, produces=False)
        w2 = WorkerForTest.spawn(count=2, some_param=1, produces=False)
        boss.setup_workers(w1, w2)
        await boss.perform_job(
            some_chunkable_data=[]
        )

    @pytest.mark.asyncio
    async def test_process_tasks_no_dead_lock(self):
        boss = BossForTest()
        w1 = WorkerForTest.spawn(count=3, some_param=2, produces=False)
        w2 = WorkerForTest.spawn(count=2, some_param=1, produces=False)
        boss.setup_workers(w1, w2)
        await boss.perform_job(
            some_chunkable_data=[1, 2, 3, 4]
        )

    @pytest.mark.asyncio
    async def test_process_tasks_with_produces_no_dead_lock(self):
        boss = BossForTest()
        w1 = WorkerForTest.spawn(count=3, some_param=2, produces=True)
        w2 = WorkerForTest.spawn(count=2, some_param=1, produces=False)
        boss.setup_workers(w1)
        w2.consume(w1)
        await boss.perform_job(
            some_chunkable_data=[1, 2, 3, 4]
        )

    @pytest.mark.asyncio
    async def test_process_tasks_with_produces_no_dead_lock_one_param(self):
        boss = BossForTest()
        w1 = WorkerForTest.spawn(count=1, some_param=2, produces=True)
        w2 = WorkerForTest.spawn(count=1, some_param=1, produces=False)
        boss.setup_workers(w1)
        w2.consume(w1)
        await boss.perform_job(
            some_chunkable_data=[1]
        )

    @pytest.mark.asyncio
    async def test_process_tasks_with_produces_exception_in_consumer(self):
        boss = BossForTest()
        w1 = WorkerForTest.spawn(count=1, some_param=2, produces=True)
        w2 = FailingWorker.spawn(count=1, some_param=1, produces=False)
        boss.setup_workers(w1)
        w2.consume(w1)
        await boss.perform_job(
            some_chunkable_data=[1]
        )
        errors = boss.get_errors()
        assert len(errors) == 1
        assert errors[0].failed_tasks[0].work_task.task_args == None

    @pytest.mark.asyncio
    async def test_process_tasks_with_produces_exception_in_producer(self):
        boss = BossForTest()
        w1 = FailingWorker.spawn(count=1, some_param=2, produces=True)
        w2 = FailingWorker.spawn(count=1, some_param=1, produces=False)
        boss.setup_workers(w1)
        w2.consume(w1)
        await boss.perform_job(
            some_chunkable_data=[1]
        )
        errors = boss.get_errors()
        assert len(errors) == 1
        assert errors[0].failed_tasks[0].work_task.task_args == [1]