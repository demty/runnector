class BasePackageException(Exception):
    """Базовая ошибка пакета"""

    pass


class NotWorkersGroupSet(BasePackageException):
    """Не установлены работники"""
    pass
