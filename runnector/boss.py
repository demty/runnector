import asyncio
import logging
from abc import ABC, abstractmethod
from typing import List, AsyncIterable

from runnector import WorkersGroup, WorkTask, WorkTaskResult, WorkTaskResultStatus
from .exceptions import NotWorkersGroupSet

logger = logging.getLogger(__name__)


class AbstractBoss(ABC):
    def __init__(self):
        self._initial_workers_groups: List[WorkersGroup] = []
        self._consume_queues: List[asyncio.Queue] = []
        self._result_queue: asyncio.Queue = asyncio.Queue()
        self._tasks_left_to_wait = 0
        self.errors = []
        self._tasks_proceed = 0
        self._tasks_failed = 0

    async def perform_job(self, *args, **kwargs) -> 'AbstractBoss':
        if not self._initial_workers_groups:
            raise NotWorkersGroupSet('Не установлены работники, нельзя делегировать работу')
        await self._wake_up_workers()
        tasks_pushed = 0
        try:
            async for task in self.prepare_data_for_workers(*args, **kwargs):
                tasks_pushed += 1
                await self._push_data_to_queue(task)
            self._tasks_left_to_wait = tasks_pushed * len(self._initial_workers_groups)
        except Exception as e:
            logger.info('Error while preparing data for workers')
            logger.exception(e)
        # self._tasks_left_to_wait = len(self._initial_workers_groups) if tasks_pushed else 0
        await self._wait_until_work_done()
        await self._send_to_sleep_workers()
        return self

    async def _push_data_to_queue(self, task: WorkTask):
        for queue in self._consume_queues:
            await queue.put(task)

    async def _wait_until_work_done(self):
        while self._tasks_left_to_wait:
            result: WorkTaskResult = await self._result_queue.get()
            self._tasks_proceed += 1
            if result.status != WorkTaskResultStatus.SUCCEED:
                self.errors.append(result)
                self._tasks_failed += 1
            self._tasks_left_to_wait -= 1
            logger.debug('tasks proceed: %s, tasks failed: %s, tasks left: %s', self._tasks_proceed,
                         self._tasks_failed, self._tasks_left_to_wait)

    async def _send_to_sleep_workers(self):
        for workers_group in self._initial_workers_groups:
            await workers_group._kill()

    def get_errors(self) -> List[WorkTaskResult]:
        return self.errors

    async def _wake_up_workers(self):
        for workers_group in self._initial_workers_groups:
            await workers_group._start()

    @abstractmethod
    async def prepare_data_for_workers(self, *args, **kwargs) -> AsyncIterable[WorkTask]:
        ...

    def setup_workers(self, *workers: WorkersGroup) -> 'AbstractBoss':
        self._initial_workers_groups = workers
        for workers_group in self._initial_workers_groups:
            q = asyncio.Queue()
            self._consume_queues.append(q)
            workers_group._result_queue = self._result_queue
            for worker in workers_group._workers:
                worker._consume_queue = q
        return self
