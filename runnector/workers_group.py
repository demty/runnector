import asyncio
from functools import reduce
from typing import List, Optional
from runnector import WorkTask, WorkTaskResultStatus, WorkTaskResult


class WorkersGroup:
    def __init__(self, produces: bool):
        self.produces = produces
        self.consumes = False
        self._tasks_for_workers_count = 0
        self._workers: List['AbstractWorker'] = []
        self._produce_queues: List[asyncio.Queue] = []
        self._result_queue: Optional[asyncio.Queue] = None
        self._workers_result_queue: Optional[asyncio.Queue] = None
        self._tasks: List[asyncio.Task] = []
        self.consumers: List['WorkersGroup'] = []
        self._task_competion_stats = {

        }

    def _add_workers(self, workers: List['AbstractWorker']):
        self._workers = workers

    async def _start(self):
        for consumer in self.consumers:
            await consumer._start()
        for worker in self._workers:
            self._tasks.append(asyncio.create_task(worker._wait_for_task()))
        if self.produces:
            self._tasks.append(asyncio.create_task(self._accept_work()))

    async def _kill(self):
        for consumer in self.consumers:
            await consumer._kill()
        for task in self._tasks:
            task.cancel()
        await asyncio.gather(*self._tasks,
                             return_exceptions=True)

    def consume(self, workers_group: 'WorkersGroup') -> 'WorkersGroup':
        q = asyncio.Queue()
        workers_group._produce_queues.append(q)
        for worker in self._workers:
            worker._consume_queue = q
        if not workers_group._workers_result_queue:
            workers_group._workers_result_queue = asyncio.Queue()
        self._result_queue = workers_group._workers_result_queue
        workers_group.consumers.append(self)
        self.consumes = True
        return self

    async def _accept_work(self):
        while True:
            task_result: WorkTaskResult = await self._workers_result_queue.get()
            await self._accept_task_result(task_result)

    async def _accept_task_result(self, task_result: WorkTaskResult):
        if task_result.status != WorkTaskResultStatus.SUCCEED:
            self._task_competion_stats[task_result.parent_task_id]['fails'].append(task_result)
        self._task_competion_stats[task_result.parent_task_id]['work_count'] -= 1
        if self._task_competion_stats[task_result.parent_task_id]['work_count'] == 0:
            await self._job_finised(self._build_result(task_result=task_result, from_deligated_task=True))

    async def _complete_task(self, task_result: WorkTaskResult, task_to_consumer: Optional[WorkTask]):
        self._work_task_completed(task_result)
        if self.produces and task_to_consumer:
            self._work_task_added(task_to_consumer, to_consumer=True)
            await self._pass_task_to_consume_group(task_to_consumer)
        if self._task_competion_stats[task_result.task_id]['work_count'] == 0 or self.consumes:
            await self._job_finised(task_result=self._build_result(task_result))

    def _work_task_completed(self, task_result: WorkTaskResult):
        self._task_competion_stats[task_result.task_id]['work_count'] -= 1
        if task_result.status != WorkTaskResultStatus.SUCCEED:
            self._task_competion_stats[task_result.task_id]['fails'].append(task_result)
        # self._tasks_for_workers_count -= 1

    def _build_result(self, task_result: WorkTaskResult, from_deligated_task: bool = False) -> WorkTaskResult:
        if from_deligated_task:
            task_id = task_result.parent_task_id
            parent_task_id = self._task_competion_stats[task_result.parent_task_id]['parent_task_id']
        else:
            task_id = task_result.task_id
            parent_task_id = task_result.parent_task_id
        fails = []
        for res_fail in self._task_competion_stats[task_id]['fails']:
            fails.extend(res_fail.failed_tasks)
        total_calls = self._task_competion_stats[task_id]['total_calls']
        if len(fails) > 0:
            if len(fails) == total_calls:
                status = WorkTaskResultStatus.FAILED
            else:
                status = WorkTaskResultStatus.PARTIALY_FAILED
        else:
            status = WorkTaskResultStatus.SUCCEED
        res = WorkTaskResult(task_id=task_id, parent_task_id=parent_task_id,
                             failed_tasks=fails, status=status)
        return res

    def _work_task_added(self, task: WorkTask, to_consumer: bool = False):
        task_id = task.task_id if not to_consumer else task.parent_task_id
        if task_id not in self._task_competion_stats:
            self._task_competion_stats[task_id] = {
                'work_count': 1,
                'total_calls': 1,
                'parent_task_id': task.parent_task_id,
                'fails': []
            }
        else:
            self._task_competion_stats[task_id]['work_count'] += 1
            self._task_competion_stats[task_id]['total_calls'] += 1
        # self._tasks_for_workers_count += 1

    async def _job_finised(self, task_result: WorkTaskResult):
        await self._result_queue.put(task_result)

    async def _pass_task_to_consume_group(self, task: WorkTask):
        for produce_queue in self._produce_queues:
            await produce_queue.put(task)
