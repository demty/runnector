import asyncio
from typing import Optional, Any, Tuple, NoReturn
from uuid import uuid4
from abc import ABC, abstractmethod

from runnector import WorkersGroup, WorkTask, WorkTaskResult, WorkTaskResultStatus
from runnector.entities import WorkTaskFail


class AbstractWorker(ABC):

    @property
    @abstractmethod
    def work_name(self) -> str:
        ...

    def __init__(self, workers_group: WorkersGroup, *args, **kwargs):
        self.id = str(uuid4())
        self.my_workers_group = workers_group
        self._consume_queue: Optional[asyncio.Queue] = None

    def work_result_to_task(self, *args, **kwargs) -> WorkTask:
        return WorkTask(task_args=args, task_kwargs=kwargs)

    async def _try_to_work(self, task: WorkTask):
        work_task_result = WorkTaskResult(task_id=task.task_id, status=WorkTaskResultStatus.PENDING,
                                          parent_task_id=task.parent_task_id)
        produced_task = None
        try:
            res = await self.work(task)
            produced_task = WorkTask(task_args=res, task_kwargs=None, parent_task_id=task.task_id)
            work_task_result.status = WorkTaskResultStatus.SUCCEED
        except Exception as e:
            work_task_result.status = WorkTaskResultStatus.FAILED
            work_task_result.failed_tasks.append(
                WorkTaskFail(work_task=task, error=e, work_name=self.work_name)
            )
        await self._tell_result(work_task_result=work_task_result, work_result=produced_task)

    async def _produce(self, produced_task: WorkTask):
        await self.my_workers_group._pass_task_to_consume_group(produced_task)

    async def _tell_result(self, work_result: Optional[WorkTask], work_task_result: WorkTaskResult):
        await self.my_workers_group._complete_task(work_task_result, task_to_consumer=work_result)

    async def _wait_for_task(self):
        while True:
            task: WorkTask = await self._consume_queue.get()
            self.my_workers_group._work_task_added(task)
            await self._try_to_work(task)

    @abstractmethod
    async def work(self, work_task: WorkTask) -> Optional[Tuple[Any, ...]] or NoReturn:
        ...

    @classmethod
    def _spawn_worker(cls, *args, **kwargs) -> 'AbstractWorker':
        return cls(*args, **kwargs)

    @classmethod
    def spawn(cls, count: int, produces: bool = False, *args, **kwargs) -> WorkersGroup:
        wg = WorkersGroup(produces=produces)
        workers = [
            cls._spawn_worker(workers_group=wg, produces=produces, *args, **kwargs) for _ in range(count)
        ]
        wg._add_workers(workers)
        return wg
