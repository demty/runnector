from enum import Enum, auto
from uuid import uuid4
from dataclasses import dataclass
from typing import Any, List, Dict, Optional, Tuple


class WorkTaskResultStatus(Enum):
    PENDING = auto()
    SUCCEED = auto()
    FAILED = auto()
    PARTIALY_FAILED = auto()


@dataclass
class WorkTask:
    task_args: Optional[List[Any]] or Optional[Tuple[Any, ...]] = None
    task_kwargs: Optional[Dict[str, Any]] = None
    task_id: Optional[str] = None
    parent_task_id: Optional[str] = None

    def __post_init__(self):
        if not self.task_id:
            self.task_id = str(uuid4())

    def __repr__(self):
        return f'{self.task_id} {self.task_args} {self.task_kwargs}'


@dataclass
class WorkTaskFail:
    work_task: WorkTask
    error: Exception
    work_name: str


@dataclass
class WorkTaskResult:
    status: WorkTaskResultStatus
    task_id: str
    parent_task_id: Optional[str] = None
    failed_tasks: Optional[List[WorkTaskFail]] = None

    def __post_init__(self):
        if not self.failed_tasks:
            self.failed_tasks = []
